<?php

namespace Models\Interfaces;

interface SaveData
{
    public function save();
}