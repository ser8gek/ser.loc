ALTER TABLE `products` CHANGE COLUMN `count` `quantity` int(11) NOT NULL;
ALTER TABLE products ADD COLUMN `images` VARCHAR(250) DEFAULT '';
ALTER TABLE `products` CHANGE COLUMN `category` `category_id` int(11) NOT NULL;
RENAME TABLE orders TO cart_prpducts;
ALTER TABLE `cart_prpducts` CHANGE COLUMN `date` `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP;
CREATE TABLE `cart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `total_price` decimal(12,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
ALTER TABLE `cart_prpducts` CHANGE COLUMN `user_id` `cart_id` int(10) unsigned NOT NULL;
ALTER TABLE cart_products ADD COLUMN `is_deleted` tinyint(1) DEFAULT '0';
ALTER TABLE `users` ADD COLUMN `status` enum ( 'Active', 'Inactive', 'Locked' ) DEFAULT 'Active';