<?php
require_once dirname(__FILE__). "/config.php";
require_once dirname(__FILE__). "/function.php";

if (!empty($_POST['login']) && !empty($_POST['password'])) {
    $response = sendLoginRequest($_POST['login'], $_POST['password']);
    if(!empty($response) && $response['success'] == 1){
        $_SESSION = $response['data'];
        if(!empty($_POST['remember'])){
            SetCookie("name", $response['data']['name'], time()+3600*24);
        }
        header("Location: /main.php");
    } else{
        $error = $response['error'];
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Form</title>
</head>
<body>
<form method="post" enctype="multipart/form-data">
    <p>Логин</p>
    <input type="text" name="login" placeholder="Логин или e-mail">
    <p>Пароль</p>
    <input type="password" name="password" placeholder="Пароль"><br/>
    <input type="checkbox" name="remember" placeholder="Пароль">Запомнить меня
    <p>
        <button type="submit" name="submit">Вход</button>
    </p>
    <?php if(!empty($error)): ?>
        <p>
            <?php echo $error; ?>
        </p>
    <?php endif; ?>
</form>
</body>
</html>
