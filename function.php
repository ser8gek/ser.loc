<?php
require_once dirname(__FILE__). "/config.php";


function checkLogin ($faleName, $login, $pass)
{

    $arrRegistr = [];
    $fparrRegistr = fopen(ROOT_PATH . "/" . "$faleName", "r");
    if (!$fparrRegistr) {
        return [];
    }
    while (!feof($fparrRegistr)) {
        $user = explode(" ", fgets($fparrRegistr));
        $arrRegistr[] = ["name" => $user[0], "login" => $user[1], "password" => $user[2], "email" => $user[3], "lang" => $user[4]];
    }
    fclose($fparrRegistr);
    foreach ($arrRegistr as $arrReg){
        if ($login==$arrReg['login'] && $pass==$arrReg['password']){
            return $arrReg;
        }
    }
    return $arrRegistr;
}

function sendLoginRequest($login, $password)
{
    $data = array("login" => $login, "password" => $password);
    $dataString = json_encode($data);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, API_URL);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//для возврата результата в виде строки, вместо прямого вывода в браузер
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
    $response = curl_exec($ch);
    curl_close ($ch);
    $response = json_decode($response, true);
    return $response;
}
?>